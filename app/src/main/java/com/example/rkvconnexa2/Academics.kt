package com.example.rkvconnexa2

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_academics.*

class Academics : Fragment(), View.OnClickListener {
    private var index:Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v = inflater.inflate(R.layout.fragment_academics, container, false)
        // Inflate the layout for this fragment


        val card1: CardView = v.findViewById(R.id.card1)
        val card2: CardView = v.findViewById(R.id.card2)
        val card3:CardView = v.findViewById(R.id.card3)
        val card4:CardView = v.findViewById(R.id.card4)
        val card5:CardView = v.findViewById(R.id.card5)
        val card7:CardView = v.findViewById(R.id.card7)
        val card8:CardView = v.findViewById(R.id.card8)



        card1.setOnClickListener(this)
        card2.setOnClickListener(this)
        card3.setOnClickListener(this)
        card4.setOnClickListener(this)
        card5.setOnClickListener(this)
        card8.setOnClickListener{
            startActivity(Intent(activity!!, Send_Email_Activity::class.java))
        }
        card7.setOnClickListener{
            startActivity(Intent(activity!!, FacultyActivity::class.java))
        }


        return v
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.card1 ->{ index = 0}
            R.id.card2 ->{ index = 1}
            R.id.card3 ->{ index = 2}
            R.id.card4 ->{ index = 3}
            R.id.card5 ->{ index = 4}
            }
        val intent = Intent(activity!!, PdfView::class.java)
        intent.putExtra("index", index)
        startActivity(intent)
    }

}
