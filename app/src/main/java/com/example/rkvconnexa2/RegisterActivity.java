package com.example.rkvconnexa2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 100;
    GoogleSignInClient mGoogleSignInClient;
    //views
    EditText mEmailEt, mPasswordEt, mNameEt, mName;
    Button mRegisterBtn, mNotHaveAcct;
    //SignInButton mGoogleLoginBtn;
    TextView mHaveAccoutnTv;
    //progress bar display
    ProgressDialog progressDialog;

    //Declare instance of fireBase
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Actionbar

        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setTitle("Create Account");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setDisplayHomeAsUpEnabled(true);

        //init
        mEmailEt = findViewById(R.id.emailEt);
        mPasswordEt = findViewById(R.id.passwordEt);
        mRegisterBtn = (Button)findViewById(R.id.registerBtn);
        mNameEt = findViewById(R.id.editEt);
        mHaveAccoutnTv = findViewById(R.id.have_accountTv);
        mName = findViewById(R.id.nameEt);
        mNotHaveAcct = findViewById(R.id.btn_notHaveAcct);

        // Configure Google Sign In
        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(RegisterActivity.this, gso);*/


        //initializing firebase Auth
        mAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering User...");
        //mGoogleLoginBtn = findViewById(R.id.googleLoginBtn);


        //handle click events
       mRegisterBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               //input email,password
               String email = mEmailEt.getText().toString().trim();
               String password = mPasswordEt.getText().toString().trim();
               String name = mName.getText().toString().trim();
               //validate
               if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                   //set error and focus to email edit text
                   mEmailEt.setError("Invalid Email");
                   mEmailEt.setFocusable(true);
               }
               else if(password.length()<6){
                   mPasswordEt.setError("Password length at least 6 character");
                   mPasswordEt.setFocusable(true);
               }
               else if(name.isEmpty()){
                   mName.setError("Enter Name");
                   mName.setFocusable(true);
               }
               else{
                   registerUser(email, password, name);
               }
           }
       });
        /*mGoogleLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //begin google login
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });*/

        //handle login Textview click
        mNotHaveAcct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });
    }

    private void registerUser(String email, String password, final String name) {

        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //sign in success, dismiiss dialog and start registaration
                            progressDialog.dismiss();
                            FirebaseUser user = mAuth.getCurrentUser();
                            //get user email and uid from auth
                            String email = user.getEmail();
                            String uid = user.getUid();
                            //when the user is registered store user info in firebase realtime
                            //using hashmap
                            HashMap<Object, String> hashMap = new HashMap<>();
                            //put info in hash map
                            hashMap.put("email", email);
                            hashMap.put("uid", uid);
                            if(!name.isEmpty()){
                                hashMap.put("name", name); }
                            else{
                                hashMap.put("name", ""); }
                            hashMap.put("phone", "");
                            hashMap.put("image", "");
                            //fire base database instance
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            //path to store user data named "user'
                            DatabaseReference reference =database.getReference("Users");
                            //put data within hashmap in database
                            reference.child(uid).setValue(hashMap);

                            Toast.makeText(RegisterActivity.this, "Registered....\n"+user.getEmail(), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            progressDialog.dismiss();
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // error dismiss progress
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
        return super.onSupportNavigateUp();
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                // ...
                Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();

                            if(task.getResult().getAdditionalUserInfo().isNewUser()){
                                String email = user.getEmail();
                                String uid = user.getUid();
                                //when the user is registered store user info in firebase realtime
                                //using hashmap
                                HashMap<Object, String> hashMap = new HashMap<>();
                                //put info in hash map
                                hashMap.put("email", email);
                                hashMap.put("uid", uid);
                                hashMap.put("name", "");
                                hashMap.put("phone", "");
                                hashMap.put("image", "");
                                //fire base database instance
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                //path to store user data named "user'
                                DatabaseReference reference =database.getReference("Users");
                                //put data within hash map in database
                                reference.child(uid).setValue(hashMap);
                            }
                            Toast.makeText(RegisterActivity.this, ""+user.getEmail() ,Toast.LENGTH_SHORT).show();

                            //go to profile
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            finish();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            //updateUI(null);
                            Toast.makeText(RegisterActivity.this, "Login Failed...", Toast.LENGTH_SHORT).show();

                        }

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }*/
}