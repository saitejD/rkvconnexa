package com.example.rkvconnexa2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_faculty.view.*
import kotlinx.android.synthetic.main.activity_write.*

class WriteActivity : AppCompatActivity() {
    private lateinit var btnPublish:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write)

        val actionBar = supportActionBar
        actionBar?.title = ""
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        val write = findViewById<EditText>(R.id.textWrite)
        btnPublish = findViewById(R.id.btnPublish)

        var title:String? = if(intent.getStringExtra("title") == "Books") {
            "Books"
        }else{
            "Poetry"
        }
        btnPublish.setOnClickListener {
            //val writing = write.text.toString()
            //Toast.makeText(this, "" + writing, Toast.LENGTH_LONG).show()
            if(write.text.toString().isEmpty()){
                write.error
                write.isFocusable = true
            }
            else{
                val intent = Intent(this@WriteActivity, UploadBooks::class.java)
                intent.putExtra("extra", write.text.toString())
                intent.putExtra("title", title!!)
                startActivity(intent)
            }

        }
    }
}
