package com.example.rkvconnexa2

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import com.google.firebase.database.FirebaseDatabase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PlacesAdapter(val mCtx: Context, val layoutResId:Int, val placesList: ArrayList<Places>, val imagesList:ArrayList<Int>)
    :ArrayAdapter<Places>(mCtx, layoutResId, placesList), CompoundButton.OnCheckedChangeListener{

    private lateinit var statusText:TextView
    private lateinit var status:TextView
    private lateinit var placeText:TextView
    private lateinit var place:TextView
    private lateinit var timeText:TextView
    private lateinit var times:TextView
    private lateinit var imageView:ImageView
    private lateinit var switchCom:SwitchCompat
    private var stat:String = ""
    private var time:String = ""

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
       val  layoutInflater:LayoutInflater = LayoutInflater.from(mCtx)
        val view:View = layoutInflater.inflate(layoutResId, null)

        placeText = view.findViewById(R.id.text1)
        statusText = view.findViewById(R.id.status)
        timeText = view.findViewById(R.id.time)
        imageView = view.findViewById(R.id.image1)

        val update = view.findViewById<Button>(R.id.update)

        val oldVal = placesList[position]

        update.setOnClickListener {
            showUpdateDialog(oldVal)
        }

        val place = placesList[position]

        statusText.text = place.status
        placeText.text = place.place
        timeText.text = place.time
        if(position>=10){ imageView.setImageResource(R.drawable.book1)}
        else{ imageView.setImageResource(imagesList[position]) }


        return view
    }


    fun showUpdateDialog(oldVal: Places) {
        val builder = AlertDialog.Builder(mCtx)
        builder.setTitle("Update Status")

        val inflater = LayoutInflater.from(mCtx)
        val view = inflater.inflate(R.layout.update_places, null)

        status = view.findViewById(R.id.status)
        times = view.findViewById(R.id.time)
        place = view.findViewById(R.id.place)
        switchCom = view.findViewById(R.id.switch2)

        place.text = oldVal.place
        status.text = oldVal.status
        times.text = oldVal.time
        builder.setView(view)

        switchCom.setOnCheckedChangeListener(this)

        status.text = stat
        times.text = time
        place.text = oldVal.place

        builder.setPositiveButton("Update") { dialog, which ->
            val refPlace = FirebaseDatabase.getInstance().getReference("Places")

            val newValues = Places(oldVal.id, oldVal.place, stat, time)

            refPlace.child(oldVal.id).setValue(newValues)
            Toast.makeText(mCtx, "Status Updated", Toast.LENGTH_SHORT).show()
            if(!switchCom.isChecked){
                val st = "Closed"
                time = getCurrentTime()
                val new = Places(oldVal.id, oldVal.place, st, time)
                refPlace.child(oldVal.id).setValue(new)
            }

        }

        builder.setNegativeButton("No"
        ) { dialog, which ->
        }

        val alert = builder.create()
        alert.show()
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentTime():String{
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("hh:mm:ss a \tdd-MM")
        time = simpleDateFormat.format(calendar.time)
        return time
    }

    /*fun showUpdateDialog(oldVal: ArrayList<Places>) {
        val builder = AlertDialog.Builder(mCtx)
        builder.setTitle("Update Status")

        val inflater = LayoutInflater.from(mCtx)
        val view = inflater.inflate(R.layout.update_places, null)

        placeText = view.findViewById<TextView>(R.id.status)
        timeText = view.findViewById<TextView>(R.id.time)
        switchCom = view.findViewById<SwitchCompat>(R.id.switch1)


        builder.setView(view)

        builder.setPositiveButton("Update"
        ) { dialog, which ->

            val refPlace = FirebaseDatabase.getInstance().getReference("Places")
            switchCom.setOnCheckedChangeListener(this)
            placeText.text = stat
            timeText.text = time
            val newStatus:String = placeText.text.toString()
            val id:String = oldVal[0].id

            val newPlace = Places(id, placeText.toString(), stat, time)
            refPlace.child(id).setValue(newPlace).addOnCompleteListener{
                Toast.makeText(mCtx, "Status Updated", Toast.LENGTH_SHORT).show()
            }

        }

        builder.setNegativeButton("No"
        ) { dialog, which -> }

        val alert = builder.create()
        alert.show()
    }*/

    override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) {
        if (switchCom.isChecked) {
            status.text = "Open"
            stat = "Open"
            time = getCurrentTime()
            times.text = time
        } else {
            status.text = "Closed"
            stat = "Closed"
            time = getCurrentTime()
            times.text = time
        }
    }
}