package com.example.rkvconnexa2

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*


import kotlinx.android.synthetic.main.activity_photography.*

class Photography : AppCompatActivity() {
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: imageAdapter? = null
    private var mProgressCircle: ProgressBar? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var mUploads: MutableList<Upload>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photography)
        val extra:Int? = intent.getIntExtra("extra", 1)
        var title:String? = null
        if(extra == 1){
            title = "Photography"
        }
        else if(extra == 0){
            title = "Drawing"
        }
        val actionBar = supportActionBar
        actionBar?.title = title

        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.layoutManager = LinearLayoutManager(this)
        mProgressCircle = findViewById(R.id.progress_circle)
        mUploads = ArrayList()
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(title!!)
        //mButton = findViewById(R.id.fab_btn)

        fab_btn.setOnClickListener{
            val i = Intent(this@Photography, UploadImage::class.java)
            i.putExtra("title", title!!)
            startActivity(i)
        }

        mDatabaseRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                    (mUploads as ArrayList<Upload>).clear()
                for (postSnapshot in dataSnapshot.children) {
                    val upload = postSnapshot.getValue(Upload::class.java)!!
                    (mUploads as ArrayList<Upload>).add(upload)
                }
                mAdapter = imageAdapter(this@Photography, mUploads as ArrayList<Upload>)
                mRecyclerView?.layoutManager= GridLayoutManager(this@Photography, 3, GridLayoutManager.VERTICAL, false)
                mRecyclerView?.run { adapter = mAdapter }
                mProgressCircle?.run { visibility = View.INVISIBLE }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@Photography, databaseError.message, Toast.LENGTH_SHORT).show()
                mProgressCircle?.run { visibility = View.INVISIBLE }
            }
        })
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}