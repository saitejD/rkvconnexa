package com.example.rkvconnexa2

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class BooksAdapter(private val mContext: Context, private val mUploads: ArrayList<BookClass>, private val title:String) :
    RecyclerView.Adapter<BooksAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.simple_list_view_item_1, parent, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return mUploads.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = mUploads.reversed()[position]
        Picasso.get().load(current.imageUrl)
            .placeholder(R.drawable.loading2)
            .fit()
            .into(holder.imageView)

        holder.text1.text = current.name
        holder.text2.text = current.summery

        holder.itemView.setOnClickListener{
            val intent = Intent(mContext, ReadActivity::class.java)
            intent.putExtra("writing",current.summery)
            intent.putExtra("Name", current.name)
            intent.putExtra("title", title)
            mContext.startActivity(intent)

        }

    }


    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        var imageView:ImageView = itemView.findViewById(R.id.image)
        var text1:TextView = itemView.findViewById(R.id.textView1)
        var text2:TextView = itemView.findViewById(R.id.textView2)

    }

    /*override fun getFilter(): Filter? {
        return filter
    }

    private val filter:Filter = object : Filter() {
        //run on background
        override fun performFiltering(charSequence: CharSequence): FilterResults {
            val filterdList: List<BookClass> = ArrayList()
            if (charSequence.toString().isEmpty()) {

            } else {
                for (movie in mUploadsAll) {
                    if (movie.toString().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                        filterdList.add(movie)
                    }
                }
            }
            val filterResults = FilterResults()
            filterResults.values = filterdResults
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mUploads.clear()
            mUploads.addAll(if (Collection <))
            String > filterResults.values
            notifyDataSetChanged()
        }
    }*/

}