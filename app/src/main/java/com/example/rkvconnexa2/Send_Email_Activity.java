package com.example.rkvconnexa2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class Send_Email_Activity extends AppCompatActivity implements View.OnClickListener {
    private EditText editTextSubject, editTextMessage;
    private TextView textViewEmail;
    private ImageView image;
    private Button btnSend, btnAttachment;
    private String email,subject,message, attachmentFile;
    private Uri URI ;
    private static final int PICK_FROM_GALLERY = 101;
    int columnIndex;
    String direct;
    File pic;
    Bitmap thumbnail;
    ProgressDialog pd;

    private static final int CAMERA_REQUEST_CODE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);

        textViewEmail = findViewById(R.id.editTextTo);
        editTextSubject = findViewById(R.id.editTextSubject);
        editTextMessage = findViewById(R.id.editTextMessage);
        btnAttachment = findViewById(R.id.buttonAttachment);
        btnSend = findViewById(R.id.buttonSend);
        image = findViewById(R.id.image_view);

        pd = new ProgressDialog(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Send Email");
        actionBar.setDisplayHomeAsUpEnabled(true);

        verifyPermissions();
        btnAttachment.setOnClickListener(this);
        btnSend.setOnClickListener(this);

    }
    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(Intent.createChooser(intent, "Complete action Using"), PICK_FROM_GALLERY);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_FROM_GALLERY && resultCode == RESULT_OK) {
            thumbnail = (Bitmap) data.getExtras().get("data");
            //Uri selectedImage = data.getData();
            image.setImageBitmap(thumbnail);
            try{
                File root = Environment.getExternalStorageDirectory();
                if(root.canWrite()){
                    pic =new File(root, "proof.png");
                    FileOutputStream out = new FileOutputStream(pic);
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.flush();
                    out.close();

                }
            } catch (IOException e) {
                Log.e("BROKEN", "Could not write file" +e.getMessage());
                Toast.makeText(this, "Could not write file", Toast.LENGTH_SHORT).show();

            }
            //image.setImageURI(selectedImage);
            //String[] filePathColumn = {MediaStore.Images.Media.DATA};

            /*Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            attachmentFile = cursor.getString(columnIndex);
            URI = Uri.parse("file://" + attachmentFile);
            image.setImageURI(URI);
            String file = Environment.getExternalStorageDirectory().getAbsolutePath();
            direct = file+attachmentFile;
            dire = new File(direct);
            cursor.close();*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onClick(View arg0) {
        if(arg0 == btnAttachment){
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, PICK_FROM_GALLERY);
        }
        if(arg0 == btnSend){
            try{
                email = "tejadande123456@gmail.com, R170733@rguktrkv.ac.in";
                subject = editTextSubject.getText().toString();
                message = editTextMessage.getText().toString();


                final Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                //emailIntent.setType("message/rfc822");
                if(!(subject.isEmpty() && message.isEmpty())){
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    Uri uri = FileProvider.getUriForFile(Objects.requireNonNull(Send_Email_Activity.this), BuildConfig.APPLICATION_ID+ ".provider_paths", pic);
                    if(uri!=null){
                        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    }
                    emailIntent.putExtra(Intent.EXTRA_TEXT, message);
                    emailIntent.setType("plain/text");
                    this.startActivities(new Intent[]{Intent.createChooser(emailIntent, "Sending Email......")});
                    //startActivity(emailIntent);
                }
                else {
                    Toast.makeText(this, "Please Enter Subject and Message", Toast.LENGTH_SHORT).show();
                }
            }
            catch (Throwable e){
                Toast.makeText(this, "Please Add Image", Toast.LENGTH_SHORT).show();
            }
        }
        //Toast.makeText(this, "Email Send Successfully", Toast.LENGTH_SHORT).show();
    }
    private void verifyPermissions(){
        String[] permissions = { Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[1]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[2]) == PackageManager.PERMISSION_GRANTED){
        }
        else {
            ActivityCompat.requestPermissions(Send_Email_Activity.this,
                    permissions,
                    CAMERA_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults){
        verifyPermissions();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
