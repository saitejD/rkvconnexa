package com.example.rkvconnexa2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_upload_image.*

class ReadActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read)

        val actionBar = supportActionBar
        actionBar?.title = "Reading"
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        incomingIntent
    }
    private val incomingIntent: Unit
        private get() {
            if (intent.hasExtra("writing")) {
                val summary = intent.getStringExtra("writing")
                val name =  intent.getStringExtra("Name")
                setItems(summary, name)
            }
        }
    private fun setItems(sum:String, na:String) {
        val summary: TextView = findViewById(R.id.read)
        val name :TextView = findViewById(R.id.name)
        summary.text = sum
        name.text = na
    }
    }
