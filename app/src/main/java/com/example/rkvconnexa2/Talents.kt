package com.example.rkvconnexa2

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import org.w3c.dom.Text


class Talents: Fragment(){
    private  var books: DatabaseReference? =null
    private  var photos:DatabaseReference? =null
    private  var poems:DatabaseReference? =null
    private  var drawings:DatabaseReference? =null

    private var mPhotos: MutableList<Upload>? = null
    private var mBooks: MutableList<BookClass>? = null
    private var mDraws:MutableList<Upload>? = null
    private var mPoems: MutableList<BookClass>? = null
    private lateinit var click1:ImageView
    private lateinit var click2:ImageView
    private lateinit var click3:ImageView
    private lateinit var click4:ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_talents, container, false)

        click1 = view.findViewById(R.id.click1)
        click2 = view.findViewById(R.id.click2)
        click3  = view.findViewById(R.id.click3)
        click4  = view.findViewById(R.id.click4)

        val img1 = view.findViewById<ImageView>(R.id.img1)
        val img2 = view.findViewById<ImageView>(R.id.img2)
        val img3 = view.findViewById<ImageView>(R.id.img3)
        val img4 = view.findViewById<ImageView>(R.id.img4)
        val text1 = view.findViewById<TextView>(R.id.text1)
        val text2 = view.findViewById<TextView>(R.id.text2)
        val text3 = view.findViewById<TextView>(R.id.text3)
        val text4 = view.findViewById<TextView>(R.id.text4)

        val img5 = view.findViewById<ImageView>(R.id.img5)
        val img6 = view.findViewById<ImageView>(R.id.img6)
        val img7 = view.findViewById<ImageView>(R.id.img7)
        val img8 = view.findViewById<ImageView>(R.id.img8)
        val text5 = view.findViewById<TextView>(R.id.text5)
        val text6 = view.findViewById<TextView>(R.id.text6)
        val text7 = view.findViewById<TextView>(R.id.text7)
        val text8 = view.findViewById<TextView>(R.id.text8)

        val img9 = view.findViewById<ImageView>(R.id.img9)
        val img10 = view.findViewById<ImageView>(R.id.img10)
        val img11 = view.findViewById<ImageView>(R.id.img11)
        val img12 = view.findViewById<ImageView>(R.id.img12)
        val text9 = view.findViewById<TextView>(R.id.text9)
        val text10 = view.findViewById<TextView>(R.id.text10)
        val text11 = view.findViewById<TextView>(R.id.text11)
        val text12 = view.findViewById<TextView>(R.id.text12)

        val img13 = view.findViewById<ImageView>(R.id.img13)
        val img14 = view.findViewById<ImageView>(R.id.img14)
        val img15 = view.findViewById<ImageView>(R.id.img15)
        val img16 = view.findViewById<ImageView>(R.id.img16)
        val text13 = view.findViewById<TextView>(R.id.text13)
        val text14 = view.findViewById<TextView>(R.id.text14)
        val text15 = view.findViewById<TextView>(R.id.text15)
        val text16 = view.findViewById<TextView>(R.id.text16)


        books = FirebaseDatabase.getInstance().getReference("Books")
        photos = FirebaseDatabase.getInstance().getReference("Photography")
        poems = FirebaseDatabase.getInstance().getReference("Poetry")
        drawings = FirebaseDatabase.getInstance().getReference("Drawing")

        mPhotos  = ArrayList()
        mBooks = ArrayList()
        mPoems = ArrayList()
        mDraws = ArrayList()
        /*val query: Query = mDatabaseRef!!.orderByChild("name")
        query.addValueEventListener(object:ValueEventListener{

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (ds: DataSnapshot in dataSnapshot.children) { //get data
                    val mImg = ds.getValue(Upload::class.java)
                    mImages.add(mImg!!)
                }
            }
            override fun onCancelled(e: DatabaseError) {
                Toast.makeText(activity, ""+e.message, Toast.LENGTH_SHORT).show()

            }
        })*/

        //For Books Images
        books!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                (mBooks as ArrayList<BookClass>).clear()
                for (postSnapshot in dataSnapshot.children) {
                    val upload = postSnapshot.getValue(BookClass::class.java)!!
                    (mBooks as ArrayList<BookClass>).add(upload) }
                //(mBooks as ArrayList<BookClass>).reversed()
                val list = (mBooks as ArrayList<BookClass>).reversed()
                Picasso.get().load(list[0].imageUrl).into(img1)
                Picasso.get().load(list[1].imageUrl).into(img2)
                Picasso.get().load(list[2].imageUrl).into(img3)
                Picasso.get().load(list[3].imageUrl).into(img4)
                text1.text = list[0].name
                text2.text = list[1].name
                text3.text = list[2].name
                text4.text = list[3].name
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(activity, databaseError.message, Toast.LENGTH_SHORT).show()
            }})


        // For Photography
        photos!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                (mPhotos as ArrayList<Upload>).clear()
                for (postSnapshot in dataSnapshot.children) {
                    val upload = postSnapshot.getValue(Upload::class.java)!!
                    (mPhotos as ArrayList<Upload>).add(upload) }
                val list = (mPhotos as ArrayList<Upload>).reversed()
                Picasso.get().load(list[0].imageUrl).into(img5)
                Picasso.get().load(list[1].imageUrl).into(img6)
                Picasso.get().load(list[2].imageUrl).into(img7)
                Picasso.get().load(list[3].imageUrl).into(img8)
                text5.text = list[0].name
                text6.text = list[1].name
                text7.text = list[2].name
                text8.text = list[3].name
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(activity, databaseError.message, Toast.LENGTH_SHORT).show()
            }})

        // For Poetry
        poems!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                (mPoems as ArrayList<BookClass>).clear()
                for (postSnapshot in dataSnapshot.children) {
                    val upload = postSnapshot.getValue(BookClass::class.java)!!
                    (mPoems as ArrayList<BookClass>).add(upload) }
                val list = (mPoems as ArrayList<BookClass>).reversed()
                Picasso.get().load(list[0].imageUrl).into(img9)
                Picasso.get().load(list[1].imageUrl).into(img10)
                Picasso.get().load(list[2].imageUrl).into(img11)
                Picasso.get().load(list[3].imageUrl).into(img12)
                text9.text = list[0].name
                text10.text = list[1].name
                text11.text = list[2].name
                text12.text = list[3].name
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(activity, databaseError.message, Toast.LENGTH_SHORT).show()
            }})

        // For Drawings
        drawings!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                (mDraws as ArrayList<Upload>).clear()
                for (postSnapshot in dataSnapshot.children) {
                    val upload = postSnapshot.getValue(Upload::class.java)!!
                    (mDraws as ArrayList<Upload>).add(upload) }
                val list = (mDraws as ArrayList<Upload>).reversed()
                Picasso.get().load(list[0].imageUrl).into(img13)
                Picasso.get().load(list[1].imageUrl).into(img14)
                Picasso.get().load(list[2].imageUrl).into(img15)
                Picasso.get().load(list[3].imageUrl).into(img16)
                text13.text = list[0].name
                text14.text = list[1].name
                text15.text = list[2].name
                text16.text = list[3].name
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(activity, databaseError.message, Toast.LENGTH_SHORT).show()
            }})

        click1.setOnClickListener {
            val intent = Intent(activity!!, BooksActivity::class.java)
            intent.putExtra("extra", 1)
            startActivity(intent) }

        click2.setOnClickListener {
            val intent = Intent(activity!!, Photography::class.java)
            intent.putExtra("extra", 1)
            startActivity(intent) }

        click3.setOnClickListener {
            val intent = Intent(activity!!, BooksActivity::class.java)
            intent.putExtra("extra", 0)
            startActivity(intent) }
        click4.setOnClickListener {
            val intent = Intent(activity!!, Photography::class.java)
            intent.putExtra("extra", 0)
            startActivity(intent)
        }

        // Inflate the layout for this fragment
        return view
    }

}

