package com.example.rkvconnexa2

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.recycler_row.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

open class RecyclerAdapter(var mCtx: Context, var placesList: ArrayList<Places>, var imagesList:ArrayList<Int>):
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>(),CompoundButton.OnCheckedChangeListener {

    private lateinit var place:ArrayList<Places>
    private lateinit var placeText:TextView
    private lateinit var timeText:TextView
    private lateinit var switchCom:SwitchCompat
    private lateinit var stat:String
    private lateinit var time:String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.recycler_row, parent, false)

        val update = view.findViewById<Button>(R.id.update)
        place = placesList


        update.setOnClickListener {
            showUpdateDialog(place)
        }
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return placesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val place = placesList[position]
        holder.bind(place)
        holder.img(imagesList)
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun bind(places:Places){
            itemView.text1.text = places.place
            itemView.status.text = places.status
            itemView.time.text = places.time
        }
        fun img(imagesList: ArrayList<Int>){
            if(position <=10){
                itemView.image1.setBackgroundResource(imagesList[position])
            }
            else{
                itemView.image1.setBackgroundResource(R.drawable.alumni_icon)
            }
        }
    }

    fun showUpdateDialog(oldVal: ArrayList<Places>) {
        val builder = AlertDialog.Builder(mCtx)
        builder.setTitle("Update Status")

        val inflater = LayoutInflater.from(mCtx)
        val view = inflater.inflate(R.layout.update_places, null)

        placeText = view.findViewById<TextView>(R.id.status)
        timeText = view.findViewById<TextView>(R.id.time)
        switchCom = view.findViewById<SwitchCompat>(R.id.switch1)


        builder.setView(view)

        builder.setPositiveButton("Update"
        ) { dialog, which ->

            val refPlace = FirebaseDatabase.getInstance().getReference("Places")
            switchCom.setOnCheckedChangeListener(this)
            placeText.text = stat
            timeText.text = time
            val newStatus:String = placeText.text.toString()
            val id:String = oldVal[0].id

            val newPlace = Places(id, placeText.toString(), stat, time)
            refPlace.child(id).setValue(newPlace).addOnCompleteListener{
                Toast.makeText(mCtx, "Status Updated", Toast.LENGTH_SHORT).show()
            }
            
        }

        builder.setNegativeButton("No"
        ) { dialog, which -> }

        val alert = builder.create()
        alert.show()
    }

    override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) {
        if (switchCom.isChecked) {
            placeText.text = "Open"
            stat = "Open"
            val calendar = Calendar.getInstance()
            val simpleDateFormat = SimpleDateFormat("hh:mm:ss a")
            time = simpleDateFormat.format(calendar.time)
            timeText.text = time
        } else {
            placeText.text = "Closed"
            stat = "Closed"
            val calendar = Calendar.getInstance()
            val simpleDateFormat = SimpleDateFormat("hh:mm:ss a")
            time = simpleDateFormat.format(calendar.time)
            timeText.text = time
        }
    }
}
