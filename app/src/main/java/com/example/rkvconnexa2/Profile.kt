package com.example.rkvconnexa2


import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage.getInstance
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.system.exitProcess

class Profile : Fragment() {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var firebaseDatabase: FirebaseDatabase
    private  lateinit var databaseReference: DatabaseReference
    private lateinit var storageReference: StorageReference
    //private lateinit var fab: FloatingActionButton
    // Initialize Firebase Auth
    //private lateinit var user:FirebaseUser
    //storage
    //path wher image of uer profile will store
    var storagePath = "Users_Profile_Images/"
    //progressdialog
    lateinit var pd: ProgressDialog
    //permission  dialog
    private val CAMERA_REQUEST_CODE = 100
    private val STOAGE_REQUEST_CODE = 200
    private val IMAGE_PICK_GALLERY_CODE = 300
    private val IMAGE_PICK_CAMERA_CODE = 400
    //array of permission
    lateinit var cameraPermissions: Array<String>
    lateinit var storagePermissions: Array<String>
    //uri of picked image
    lateinit var image_uri: Uri
    //for checking profile or cover
    var profilePhoto: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val intent = inflater.inflate(R.layout.fragment_profile, container, false)
        val aboutUs = intent.findViewById<Button>(R.id.aboutUs)

        firebaseAuth = FirebaseAuth.getInstance()
        val user = FirebaseAuth.getInstance().currentUser
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Users")
        storageReference = getInstance().reference

        //init views
        val avatarTv = intent.findViewById<ImageView>(R.id.avatarTv)
        val nameTv = intent.findViewById<TextView>(R.id.nameTv)
        val emailTv = intent.findViewById<TextView>(R.id.emailTv)
        val phoneTv = intent.findViewById<TextView>(R.id.phoneTv)
        val cardTv = intent.findViewById<CardView>(R.id.cardTv)

        cardTv.setOnClickListener{
            showImagePicDialog()
        }


        val query: Query = databaseReference.orderByChild("email").equalTo(user?.email)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (ds: DataSnapshot in dataSnapshot.children) { //get data
                    val name = "" + ds.child("name").value
                    val email = "" + ds.child("email").value
                    val phone = "" + ds.child("phone").value
                    val image = "" + ds.child("image").value
                    //set data
                    nameTv.text = name
                    emailTv.text = email
                    phoneTv.text = phone
                    try { //if image is received then set
                        Picasso.get().load(image).placeholder(R.drawable.alumni_icon).into(avatarTv)
                    } catch (e: Exception) { //if there is any exception
                        Picasso.get().load(R.drawable.alumni_icon).into(avatarTv)
                    }
                }
            }
            override fun onCancelled(e: DatabaseError) {
               // Toast.makeText(activity, ""+e.message, Toast.LENGTH_SHORT).show()
            }
        })
        aboutUs.setOnClickListener {
            val i = Intent(activity, AboutUs::class.java)
            startActivity(i)

            //firebaseAuth.signOut()
            //checkUserStatus()
            //exitProcess(0)

        }

        val fab = intent.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener{ showEditProfileDialog() }
        //init array of permissions
        //init array of permissions
        cameraPermissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        storagePermissions =
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        pd = ProgressDialog(activity)
        return intent
    }


    /*private fun checkUserStatus() { //get current user
        val user = firebaseAuth.currentUser
        if (user != null) {
            //user is signed in stay here
            //set email of user
            //profileTv.setText(user.email).toString()
        } else {
            val i = Intent(activity, HomeActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
            exitProcess(0)

        }
    }*/

    private fun checkStoragePermission(): Boolean { //check if storage permission is enabled or not
        return (ContextCompat.checkSelfPermission(
            activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestStoragePermission() { //request runtime
        requestPermissions(storagePermissions, STOAGE_REQUEST_CODE)
    }


    private fun checkCameraPermission(): Boolean { //check if storage permission is enabled or not
        val result =
            (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED)
        val result1 = (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        return result && result1
    }

    private fun requestCameraPermission() { //request runtime
        requestPermissions(cameraPermissions, CAMERA_REQUEST_CODE)
    }


    private fun showEditProfileDialog() { //options to show in dialog
        val options =
            arrayOf("Edit Profile Picture", "Edit Name", "Edit Phone")
        val builder = AlertDialog.Builder(activity!!)
        //set title
        builder.setTitle("Choose Action")
        //ser item to dialog
        builder.setItems(options) { dialog, which ->
            //handle dialog item clicks
            if (which == 0) { //edit profile clicked
                pd.setMessage("Updating Profile Picture")
                profilePhoto = "image"
                showImagePicDialog()
            } else if (which == 1) { //edit Name clicked
                pd.setMessage("Updating Name")
                showNamePhoneUpdateDialog("name")
            } else if (which == 2) { //edit phone clicked
                pd.setMessage("Updating Phone")
                showNamePhoneUpdateDialog("phone")
            }
        }
        //create add show dialog
        builder.create().show()
    }


    private fun pickFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, IMAGE_PICK_GALLERY_CODE)

    }

    private fun pickFromCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "Temp Pic")
        values.put(MediaStore.Images.Media.DESCRIPTION, "Temp Description")
        //put image uri
        image_uri = activity!!.contentResolver
            .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
        //intent to start
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE)
    }



    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_PICK_GALLERY_CODE) { //image is picked from gallery
                image_uri = data?.data!!
                uploadProfilePhoto(image_uri)
            }
            if (requestCode == IMAGE_PICK_CAMERA_CODE) { //image is picked form camera,
                uploadProfilePhoto(image_uri)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }



    private fun uploadProfilePhoto(uri: Uri) {
        pd.setMessage("Uploading")
        pd.show()
        //path and name of image to be stored in firebase storage
        val user = FirebaseAuth.getInstance().currentUser
        val filePathAndName = storagePath + "" + profilePhoto + "_" + user?.uid
        val storageReference2nd = storageReference.child(filePathAndName)
        storageReference2nd.putFile(uri)
            .addOnSuccessListener { taskSnapshot ->
                //there were some errors
                val uriTask =
                    taskSnapshot.storage.downloadUrl
                while (!uriTask.isSuccessful);
                val downloadUri = uriTask.result
                //check if image is uploaded or not
                if (uriTask.isSuccessful) { //image uploaded
                    //add upload url in user's database
                    val results = HashMap<String, Any>()
                    results[profilePhoto] = downloadUri.toString()
                    val user = FirebaseAuth.getInstance().currentUser
                    user?.getUid()?.let {
                        databaseReference.child(it).updateChildren(results)
                            .addOnSuccessListener(OnSuccessListener<Void?> {
                                pd.dismiss()
                                Toast.makeText(activity, "Image Updated", Toast.LENGTH_SHORT).show()
                            })
                            .addOnFailureListener{
                                pd.dismiss()
                                Toast.makeText(activity, "Error Updating Image",Toast.LENGTH_SHORT).show()
                            }
                    }
                } else { //error
                    pd.dismiss()
                    Toast.makeText(activity, "Some error occurred", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { e ->
                pd.dismiss()
                Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
            }
    }



    private fun showImagePicDialog() {
        val options = arrayOf("Camera", "Gallery")
        val builder = AlertDialog.Builder(activity!!).also {
            //set title
            it.setTitle("Pick Image")
            //ser item to dialog
            it.setItems(
                options,
                object : DialogInterface.OnClickListener {
                    override fun onClick(
                        dialog: DialogInterface,
                        which: Int
                    ) { //handle dialog item clicks
                        if (which == 0) { //Camera clicked
                            if (!checkCameraPermission()) {
                                requestCameraPermission()
                            } else {
                                pickFromCamera()
                            }
                        } else if (which == 1) { //gallery clicked
                            if (!checkStoragePermission()) {
                                requestStoragePermission()
                            } else {
                                pickFromGallery()
                            }
                        }
                    }

                }
            )
        }
        builder.create().show()
    }

    private fun showNamePhoneUpdateDialog(key: String) { //custom dialog
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle("Update $key") // eg. update name or phone
        //set layout of dialog
        val linearLayout = LinearLayout(activity)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.setPadding(10, 10, 10, 10)
        //add edit text
        val editText = EditText(activity)
        editText.hint = "Enter $key"
        linearLayout.addView(editText)
        builder.setView(linearLayout)
        //ad buttons in dailog
        builder.setPositiveButton("Update") { dialog, which ->
            val value = editText.text.toString().trim { it <= ' ' }
            if (!TextUtils.isEmpty(value)) {
                pd.show()
                val result = HashMap<String, Any>()
                result[key] = value
                val user = FirebaseAuth.getInstance().currentUser
                user?.getUid()?.let {
                    databaseReference.child(it).updateChildren(result)
                        .addOnSuccessListener {
                            pd.dismiss()
                            Toast.makeText(activity, "Updated...", Toast.LENGTH_SHORT).show()
                        }
                        .addOnFailureListener{e ->
                            pd.dismiss()
                            Toast.makeText(activity, "" + e.message, Toast.LENGTH_SHORT).show()
                        }
                }
            } else {
                Toast.makeText(activity, "Please enter $key", Toast.LENGTH_SHORT).show()
            }}
            builder.setNegativeButton("Cancel") {
                    dialog, which -> dialog.dismiss() }
            //create and show dialog
            builder.create().show()

    }
}

