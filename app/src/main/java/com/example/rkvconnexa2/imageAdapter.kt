package com.example.rkvconnexa2

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class imageAdapter(private val mContext: Context, private val mUploads: List<Upload>) : RecyclerView.Adapter<imageAdapter.ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.image_item, parent, false)
        return ImageViewHolder(v)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val uploadCurrent = mUploads.reversed()[position]
        //holder.textViewName.text = uploadCurrent.name
        Picasso.get()
            .load(uploadCurrent.imageUrl)
            .placeholder(R.drawable.load)
            .fit()
            .into(holder.imageView)


        holder.parentLayout.setOnClickListener{
            val intent = Intent(mContext, Gallery::class.java)
            intent.putExtra("image_url", uploadCurrent.imageUrl)
            intent.putExtra( "name", uploadCurrent.name)
            mContext.startActivity(intent)

        }
    }


    override fun getItemCount(): Int {
        return mUploads.size
    }

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val parentLayout = itemView

        //var textViewName: TextView = itemView.findViewById(R.id.text_view_name)
        var imageView: ImageView = itemView.findViewById(R.id.image_view_upload)

    }
}