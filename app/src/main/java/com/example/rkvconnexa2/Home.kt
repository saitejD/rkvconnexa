package com.example.rkvconnexa2

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.SUNDAY
import java.util.Calendar.getInstance


class Home : Fragment() {

    var time:String = ""
    private lateinit var wish: TextView
    private lateinit var Username: TextView

    private lateinit var firebaseAuth: FirebaseAuth
    //private lateinit var user:FirebaseUser
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var firebaseStorage: FirebaseStorage
    private lateinit var databaseReference: DatabaseReference
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.fragment_home, container, false)

        firebaseAuth = FirebaseAuth.getInstance()
        val user  = FirebaseAuth.getInstance().currentUser
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseDatabase = FirebaseDatabase.getInstance()
        firebaseStorage = FirebaseStorage.getInstance()

        wish = view.findViewById(R.id.wish)
        Username = view.findViewById(R.id.name)

        val breakfast = view.findViewById<TextView>(R.id.breakfast)
        val lunch = view.findViewById<TextView>(R.id.lunch)
        val snacks = view.findViewById<TextView>(R.id.snacks)
        val dinner = view.findViewById<TextView>(R.id.dinner)



        checkConnection()
        val card1 = view.findViewById<CardView>(R.id.card2)
        card1.setOnClickListener {
            startActivity(Intent(activity!!, Places_Rkv_Activity::class.java)) }

        when (getCurrentTime()) {
            in 5..11 -> {
                wish.text = "Good Morning" }
            in 11..16 -> {
                wish.text = "Good Afternoon" }
            in 16..21 -> {
                wish.text = "Good Evening" }
            else -> {
                wish.text = "Good Night" }
        }

        databaseReference = firebaseDatabase.getReference("Users")
        val query: Query = databaseReference.orderByChild("email").equalTo(user?.email)
        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (ds: DataSnapshot in dataSnapshot.children) { //get data
                    val name = "" + ds.child("name").value
                    try { //if image is received then set
                        Username.text = name
                    } catch (e: Exception) { //if there is any exception
                        Username.text = "No Name"
                    }
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
              //Toast.makeText(context, databaseError.message, Toast.LENGTH_SHORT).show()
                }
        })


        var day:String = ""
        val dayformat = SimpleDateFormat("EEEE", Locale.US)
        val calendar:Calendar = getInstance()
        day = dayformat.format(calendar.time)
        when(day){
            "SUNDAY" ->{day = "Sunday" }
            "MONDAY" ->{day = "Monday"}
            "TUESDAY" ->{day = "Tuesday"}
            "WEDNESDAY"->{day = "Wednesday"}
            "THURSDAY"->{day = "Thursday"}
            "FRIDAY" ->{day = "Friday"}
            "SATURDAY"->{day = "Saturday"}
        }
        //Toast.makeText(activity!!, "Day = $day", Toast.LENGTH_SHORT).show()

        val ref = firebaseDatabase.getReference("Mess").child(day)
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                val b:String = p0.child("Breakfast").value.toString()
                val l:String = p0.child("Lunch").value.toString()
                val s:String = p0.child("Snacks").value.toString()
                val d:String = p0.child("Dinner").value.toString()

                breakfast.text = b
                lunch.text = l
                snacks.text = s
                dinner.text = d }
            override fun onCancelled(p0: DatabaseError) {
//                Toast.makeText(activity, p0.message, Toast.LENGTH_SHORT).show()
            }
        })

        return view
    }

    private fun checkConnection() {
        val manager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = manager.activeNetworkInfo
        if (null == activeNetwork) {
            Toast.makeText(activity, "No Internet Connection", Toast.LENGTH_LONG).show() }
    }


    fun getCurrentTime():Int{
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.HOUR_OF_DAY)
    }
}

