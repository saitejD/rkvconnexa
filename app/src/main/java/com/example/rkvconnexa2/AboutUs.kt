package com.example.rkvconnexa2

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth

class AboutUs : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        val appBar = supportActionBar
        appBar?.title = "About Us"
        appBar?.setDisplayHomeAsUpEnabled(true)

        val logOut = findViewById<Button>(R.id.logout)
        val contact = findViewById<TextView>(R.id.contactUs)
        logOut.setOnClickListener {
            val user = FirebaseAuth.getInstance().currentUser
            FirebaseAuth.getInstance().signOut()
            finish()
            startActivity(Intent(this@AboutUs, HomeActivity::class.java))

        }
        contact.setOnClickListener {
            alert()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun alert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Contact Us")

        builder.setMessage("tejadande123456@gmail.com\nnaveenbhargav108@gmail.com")

        builder.setNeutralButton("Okay", null)
        val dialog:AlertDialog = builder.create()
        dialog.show()
    }

}
