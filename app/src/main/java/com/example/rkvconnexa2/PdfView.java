package com.example.rkvconnexa2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PdfView extends AppCompatActivity {
    String url = "";
    //TextView text1;
    //PDFView pdfView;

    //private ProgressBar pb;
    private List<String> mUploads;
    private DatabaseReference mDatabaseRef;
    private FirebaseStorage mStorageRef = FirebaseStorage.getInstance();
    private StorageReference academicsReference = mStorageRef.getReferenceFromUrl("gs://rkvconnexa2.appspot.com").child("Academics");
    private StorageReference facultyReference = mStorageRef.getReferenceFromUrl("gs://rkvconnexa2.appspot.com").child("Facutly");
    //private StorageReference facultyReference = mStorageRef.getReferenceFromUrl()
    private PhotoView imgView;
    private List<String> mFaculty;
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_view);

        ActionBar actionBar = getSupportActionBar();
        //assert actionBar != null;
        actionBar.setTitle(" ");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        imgView = findViewById(R.id.imgView);
        imgView.setScaleType(PhotoView.ScaleType.FIT_XY);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading....");
        pd.show();
        //text1 = findViewById(R.id.text1);
        //pb = findViewById(R.id.pb);
        mUploads = new ArrayList<>();
        mUploads.add("administration.png");
        mUploads.add("academicCalendar.png");
        mUploads.add("hospital.png");
        mUploads.add("mess.png");
        mUploads.add("offices.png");

        mFaculty = new ArrayList<>();
        mFaculty.add("depart_biology.png");
        mFaculty.add("depart_chemical.png");
        mFaculty.add("depart_chemistry.png");
        mFaculty.add("depart_civil.png");
        mFaculty.add("depart_cse.png");
        mFaculty.add("depart_ece.png");
        mFaculty.add("depart_eee.png");
        mFaculty.add("depart_english.png");
        mFaculty.add("depart_arts.png");
        mFaculty.add("depart_it.png");
        mFaculty.add("depart_library.png");
        mFaculty.add("depart_maths.png");
        mFaculty.add("depart_mech.png");
        mFaculty.add("depart_mme.png");
        mFaculty.add("depart_sports.png");
        mFaculty.add("depart_physics.png");
        mFaculty.add("depart_telugu.png");


        //mDatabaseRef = FirebaseDatabase.getInstance().getReference("Academics");
        //mStorageRef = FirebaseStorage.getInstance().getReference("Pdf");




        int index = Integer.parseInt(getIntent().getExtras().get("index").toString());
        if (index == 0) {
            url = mUploads.get(0);
        } else if (index == 1) {
            url = mUploads.get(1);
        } else if(index == 2){
            url = mUploads.get(2);
        }
        else if(index == 3){
            url = mUploads.get(3);}
        else if(index == 4){
            url = mUploads.get(4);
        }
        //mFaculty start
        else if(index ==5){
            url = mFaculty.get(0);
        }
        else if(index ==6){
            url = mFaculty.get(1);
        }
        else if(index ==7 ){
            url = mFaculty.get(2);
        }
        else if(index ==8){
            url = mFaculty.get(3);
        }
        else if(index ==9){
            url = mFaculty.get(4);
        }
        else if(index ==10){
            url = mFaculty.get(5);
        }
        else if(index ==11){
            url = mFaculty.get(6);
        }
        else if(index ==12){
            url = mFaculty.get(7);
        }
        else if(index ==13){
            url = mFaculty.get(8);
        }
        else if(index ==14){
            url = mFaculty.get(9);
        }
        else if(index ==15){
            url = mFaculty.get(10);
        }
        else if(index ==16){
            url = mFaculty.get(11);
        }
        else if(index ==17){
            url = mFaculty.get(12);
        }
        else if(index ==18){
            url = mFaculty.get(13);
        }
        else if(index ==19){
            url = mFaculty.get(14);
        }
        else if(index ==20){
            url = mFaculty.get(15);
        }
        else if(index ==21){
            url = mFaculty.get(16);
        }


        //final long ONE_MEGABYTE = 1024 * 1024;

            try {
                    final File file = File.createTempFile("image", "png");

                    StorageReference chil = null;
                    if(index<=4){
                        chil = academicsReference;
                    }
                    else{
                        chil = facultyReference;
                    }
                    chil.child(url).getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            pd.dismiss();
                            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                            imgView.setImageBitmap(bitmap);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            pd.dismiss();
                            Toast.makeText(PdfView.this, "File Failed to Load", Toast.LENGTH_SHORT).show();
                        }
                    });
            } catch (IOException e) {
                e.printStackTrace();
            }
        /*pb.setVisibility(View.VISIBLE);
        mStorageRef.child(url).getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Picasso.get().load(url).into(imgView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Toast.makeText(PdfView.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }
        @Override
        public boolean onSupportNavigateUp() {
            onBackPressed();
            return true;
        }

        /*mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String , Object>) dataSnapshot.getValue();
                //Long value = dataSnapshot.getValue(Long.class);
                //assert map != null;
                new PdfView.RetrievePDFStream().execute(map.toString());
                /*for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //pdf  upload = postSnapshot.getValue(pdf.class);
                    //mUploads.add(upload);
                    value = postSnapshot.getValue(String.class);

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(PdfView.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });*/
        //Intent intent = getIntent();


        //new PdfView.RetrievePDFStream().execute(value);

        //if (index == 0 && mUploads!= null && mUploads.size()!=0) {
          //  new PdfView.RetrievePDFStream().execute(value);}
        //else if (index == 1) {
        //    new PdfView.RetrievePDFStream().execute(mUploads.get(1).toString()); }

    /*class RetrievePDFStream extends AsyncTask<String, Void, InputStream> {
        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            pdfView.fromStream(inputStream).load();
        }
    }*/
}

