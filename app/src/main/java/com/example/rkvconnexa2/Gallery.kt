package com.example.rkvconnexa2

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_gallery.*
import kotlinx.android.synthetic.main.activity_upload_image.*
import kotlinx.android.synthetic.main.activity_upload_image.image_view
import kotlinx.android.synthetic.main.activity_upload_image.rotate
import org.w3c.dom.Text

class Gallery : AppCompatActivity() {
    private var mUploads: MutableList<Upload>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)
        incomingIntent

        val actionBar = supportActionBar
        actionBar?.title = "Gallery"

        rotate.setOnClickListener{ image_view.rotation = image_view.rotation + 90 }

    }
        private val incomingIntent: Unit
        private get() {
            if (intent.hasExtra("image_url")) {
                val imageUrl = intent.getStringExtra("image_url")
                val name = intent.getStringExtra("name")
                setItems(imageUrl, name)
            }
        }
        private fun setItems(imageUrl: String, Name:String) {
            val image = findViewById<ImageView>(R.id.image_view)
            val textView:TextView = findViewById(R.id.textView)
            Picasso.get()
                .load(imageUrl)
                .into(image)
            textView.text = Name
        }
}