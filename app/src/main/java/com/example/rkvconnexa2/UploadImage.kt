package com.example.rkvconnexa2

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_upload_image.*

class UploadImage : AppCompatActivity() {
    private var mButtonChooseImage: Button? = null
    private var mButtonUpload: Button? = null
    private var mEditTextFileName: TextView? = null
    private lateinit var mImageView: ImageView
    private var mProgressBar: ProgressBar? = null
    private lateinit var mRotate: ImageView
    private var mImageUri: Uri? = null
    private var mStorageRef: StorageReference? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var mUploadTask: StorageTask<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_image)

        //Actionbar
        val actionBar = supportActionBar
        actionBar!!.title = "Uploading"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

        mButtonChooseImage = findViewById(R.id.button_choose_image)
        mButtonUpload = findViewById(R.id.button_upload)
        mEditTextFileName = findViewById(R.id.edit_text_file_name)
        mImageView = findViewById(R.id.image_view)
        mProgressBar = findViewById(R.id.progress_bar)
        mRotate = findViewById(R.id.rotate)

        var title:String? = if(intent.getStringExtra("title") == "Photography") {
            "Photography"
        }else{
            "Drawing"
        }

        mStorageRef = FirebaseStorage.getInstance().getReference(title!!)
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(title!!)
        button_choose_image.setOnClickListener(View.OnClickListener { openFileChooser() })

        button_upload.setOnClickListener{
            if (mUploadTask != null && mUploadTask!!.isInProgress) {
                Toast.makeText(this@UploadImage, "Upload in progress", Toast.LENGTH_SHORT).show()
            } else {
                uploadFile()
            }
        }
        mRotate.setOnClickListener {
            mImageView.rotation = mImageView.rotation + 90
        }



    }
    private fun openFileChooser() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            mImageUri = data.data
            Picasso.get().load(mImageUri).into(mImageView)
        }
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR = contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    private fun uploadFile() {
        if (mImageUri != null) {
            val fileReference = mStorageRef!!.child(System.currentTimeMillis()
                .toString() + "." + getFileExtension(mImageUri!!))
            mUploadTask = fileReference.putFile(mImageUri!!)
                .addOnSuccessListener { taskSnapshot ->
                    val handler = Handler()
                    handler.postDelayed({ mProgressBar!!.progress = 0 }, 500)
                    Toast.makeText(this@UploadImage, "Upload successful", Toast.LENGTH_LONG).show()

                    val urlTask = taskSnapshot.storage.downloadUrl
                    while (!urlTask.isSuccessful);
                    val downloadUrl = urlTask.result
                    //Log.d(TAG,"onSuccess:firebase download url:" + downloadUrl.toString());
                    val upload = Upload(mEditTextFileName!!.text.toString().trim { it <= ' ' }, downloadUrl.toString())
                    val uploadId = mDatabaseRef!!.push().key
                    mDatabaseRef!!.child(uploadId!!).setValue(upload)

                    //mImageView.removeCallbacks()
                    /*Upload upload = new Upload(mEditTextFileName.getText().toString().trim(),
                                            taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());
                                    String uploadId = mDatabaseRef.push().getKey();
                                    mDatabaseRef.child(uploadId).setValue(upload);*/
                }
                .addOnFailureListener { e -> Toast.makeText(this@UploadImage, e.message, Toast.LENGTH_SHORT).show() }
                .addOnProgressListener { taskSnapshot ->
                    val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount
                    mProgressBar!!.progress = progress.toInt()
                }
        } else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        private const val PICK_IMAGE_REQUEST = 1
    }
}