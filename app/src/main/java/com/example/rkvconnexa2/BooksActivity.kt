package com.example.rkvconnexa2

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import java.util.*
import kotlin.collections.ArrayList

class BooksActivity : AppCompatActivity() {
    private lateinit var fab:FloatingActionButton
    private var mUploads: MutableList<BookClass>? = null
    private lateinit var mUploadsAll: ArrayList<BookClass>
    private lateinit var list:ArrayList<BookClass>


    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: BooksAdapter? = null
    private var mProgressCircle: ProgressBar? = null
    private var mDatabaseRef: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_books)

        val extra:Int? = intent.getIntExtra("extra", 1)

        var title:String? = null
        if(extra == 1){
            title = "Books"
        }
        else if(extra == 0){
            title = "Poetry"
        }

        val actionBar = supportActionBar
        actionBar?.title = title
        mUploads = ArrayList()
        mUploadsAll = ArrayList()
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)

        fab = findViewById(R.id.fab_btn)
        mRecyclerView = findViewById(R.id.bookRecycler)
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.layoutManager = LinearLayoutManager(this)
        mProgressCircle = findViewById(R.id.progress_circle)
        mUploads = ArrayList()
        mUploadsAll = ArrayList()
        list = ArrayList()
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(title!!)

        fab.setOnClickListener{
            val intent = Intent(this@BooksActivity, WriteActivity::class.java)
            intent.putExtra("title", title!!)
            startActivity(intent)
        }

        mDatabaseRef!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                (mUploads as ArrayList<BookClass>).clear()
                for (postSnapshot in dataSnapshot.children) {
                    val upload = postSnapshot.getValue(BookClass::class.java)!!
                    (mUploads as ArrayList<BookClass>).add(upload)
                    list.add(upload)
                }
                //Toast.makeText(this@BooksActivity, list.size, Toast.LENGTH_SHORT).show()
                mUploadsAll.addAll(list)
                //Toast.makeText(this@BooksActivity, "size ${mUploadsAll.size}.", Toast.LENGTH_SHORT).show()
                mAdapter = BooksAdapter(this@BooksActivity, mUploads as ArrayList<BookClass>, title)
                mRecyclerView!!.adapter = mAdapter
                mProgressCircle?.run { visibility = View.INVISIBLE }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(this@BooksActivity, databaseError.message, Toast.LENGTH_SHORT).show()
                mProgressCircle?.run { visibility = View.INVISIBLE }
            }
        })

    }

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val item:MenuItem = menu!!.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView
        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if(newText.isNotEmpty()){
                    mUploadsAll.clear()
                    val search = newText.toLowerCase(Locale.getDefault())
                    mUploads!!.forEach {

                        if(it.name.toLowerCase(Locale.getDefault()).contains(search)){
                            mUploadsAll.add(it)
                        }
                    }
                    mRecyclerView!!.adapter!!.notifyDataSetChanged()
                }
                else{
                    mUploadsAll.clear()
                    mUploadsAll.addAll(list)
                    mRecyclerView!!.adapter!!.notifyDataSetChanged()
                }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }*/

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
