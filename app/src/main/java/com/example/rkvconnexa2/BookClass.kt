package com.example.rkvconnexa2

class BookClass {
    var name: String = ""
    var imageUrl: String = ""
    var summery:String = ""

    constructor() { //empty constructor needed
    }

    constructor(name: String, imageUrl: String, summery:String) {
        var name = name
        if (name.trim { it <= ' ' } == "") {
            name = "No Name"
        }
        this.name = name
        this.imageUrl = imageUrl
        this.summery = summery
    }
}