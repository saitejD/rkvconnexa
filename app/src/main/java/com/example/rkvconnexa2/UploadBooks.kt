package com.example.rkvconnexa2

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.webkit.MimeTypeMap
import android.widget.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.squareup.picasso.Picasso

class UploadBooks : AppCompatActivity() {

    private lateinit var coverImg:ImageView
    private lateinit var bookName:EditText
    private lateinit var bookSummary:TextView
    private lateinit var btnUpload:Button
    private var mImageUri: Uri? = null
    private lateinit var progressBar:ProgressBar

    private var mStorageRef: StorageReference? = null
    private var mDatabaseRef: DatabaseReference? = null
    private var mUploadTask: StorageTask<*>? = null
    private var writing:String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_books)

        coverImg = findViewById(R.id.bookImg)
        bookName = findViewById(R.id.bookName)
        bookSummary = findViewById(R.id.bookSummery)
        btnUpload = findViewById(R.id.btnUpload)
        progressBar = findViewById(R.id.progress_bar)

        val actionBar = supportActionBar
        actionBar?.title = "Upload Writing"
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)

        val name = bookName.text
        coverImg.setOnClickListener { openFileChooser() }


        if(intent.hasExtra("extra")){
            writing = intent.getStringExtra("extra")
            bookSummary.text = writing
            //Toast.makeText(this@UploadBooks, bookSummary.text.toString(), Toast.LENGTH_SHORT).show()
        }


        btnUpload.setOnClickListener {
            if(name.isEmpty()){
                bookName.error = "Enter Writing Name!"
                bookName.isFocusable = true
            }
            else if(writing!!.isEmpty()){
                bookSummary.error = "No text to Upload"
                bookSummary.isFocusable = true
            }
            else if (mUploadTask != null && mUploadTask!!.isInProgress) {
                Toast.makeText(this@UploadBooks, "Upload in progress", Toast.LENGTH_SHORT).show()
            } else {
                uploadFile()
            }
        }
        var title:String? = if(intent.getStringExtra("title") == "Books") {
            "Books"
        }else{
            "Poetry"
        }
        mStorageRef = FirebaseStorage.getInstance().getReference(title!!)
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(title)
    }

    private fun openFileChooser() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            mImageUri = data.data
            Picasso.get().load(mImageUri).into(coverImg)
        }
    }

    private fun getFileExtension(uri: Uri): String? {
        val cR = contentResolver
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(cR.getType(uri))
    }

    private fun uploadFile() {
        if (mImageUri != null) {
            val fileReference = mStorageRef!!.child(System.currentTimeMillis()
                .toString() + "." + getFileExtension(mImageUri!!))
            mUploadTask = fileReference.putFile(mImageUri!!)
                .addOnSuccessListener { taskSnapshot ->
                    val handler = Handler()
                    handler.postDelayed({ progressBar.progress = 0 }, 500)
                    Toast.makeText(this@UploadBooks, "Upload successful", Toast.LENGTH_LONG).show()
                    val urlTask = taskSnapshot.storage.downloadUrl
                    while (!urlTask.isSuccessful);
                    val downloadUrl = urlTask.result
                    //Log.d(TAG,"onSuccess:firebase download url:" + downloadUrl.toString());
                    val upload = BookClass( bookName.text.toString(),downloadUrl.toString(), bookSummary.text.toString())
                    val uploadId = mDatabaseRef!!.push().key
                    mDatabaseRef!!.child(uploadId!!).setValue(upload)

                }
                .addOnFailureListener { e -> Toast.makeText(this@UploadBooks, e.message, Toast.LENGTH_SHORT).show() }
                .addOnProgressListener { taskSnapshot ->
                    val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount
                    progressBar.progress = progress.toInt()
                }
        } else {
            Toast.makeText(this, "Add Cover Image", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        private const val PICK_IMAGE_REQUEST = 1
    }

}
