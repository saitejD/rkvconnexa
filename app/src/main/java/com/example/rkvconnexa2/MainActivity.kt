package com.example.rkvconnexa2

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity(),  BottomNavigationView.OnNavigationItemSelectedListener{

        var bottomNavigationView: BottomNavigationView? = null
        private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //init
        firebaseAuth = FirebaseAuth.getInstance()

        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView!!.setOnNavigationItemSelectedListener(this as BottomNavigationView.OnNavigationItemSelectedListener)
        bottomNavigationView!!.selectedItemId = R.id.navigation_home

    }
        var home = Home()
        var academics = Academics()
        var talents = Talents()
        var profile = Profile()

        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.navigation_home -> {
                    supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.container, home).commit()

                    return true
                }
                R.id.navigation_academics -> {
                    supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.container, academics).commit()
                    return true
                }
                R.id.navigation_talents -> {
                    supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.container, talents).commit()
                    return true
                }
                R.id.navigation_profile -> {
                    supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.container, profile).commit()
                    return true
                }
            }
            return false
        }

    private fun checkUserStatus() { //get current user
        val user = firebaseAuth.currentUser
        if (user !=null) {
            //user is signed in stay here
            //ser email of user
            //profileTv.setText(user.email).toString()
        }
        else{
            startActivity(Intent(this@MainActivity, HomeActivity::class.java))
            finish()
        }
    }
    override fun onStart(){ //check on start of app
        checkUserStatus()
        super.onStart()

    }
    override fun onBackPressed() {
            super.onBackPressed()
            exitProcess(0)
    }

}


