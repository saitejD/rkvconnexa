package com.example.rkvconnexa2

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Places_Rkv_Activity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener{
    private lateinit var place: EditText
    private lateinit var currentTime:TextView
    private lateinit var status:TextView
    private lateinit var switch1:SwitchCompat
    private lateinit var fab_btn:FloatingActionButton
    private var time:String = ""
    private var newPlace:String = ""
    private var stat:String = "Closed"
    private var listView:ListView? = null
    private var ref: DatabaseReference? = null
    private lateinit var pd:ProgressDialog
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_places__rkv_)

        listView = findViewById(R.id.listView)
        fab_btn = findViewById(R.id.fab_btn)

        val actionBar = supportActionBar
        actionBar!!.title = "Rkv Places"
        actionBar.setDisplayHomeAsUpEnabled(true)

        pd = ProgressDialog(this)
        pd.setMessage("Loading...")
        pd.show()

        val images = arrayListOf(
            R.drawable.book1, R.drawable.book2, R.drawable.book3, R.drawable.book4,R.drawable.book5,
            R.drawable.book6, R.drawable.book7,R.drawable.book8,R.drawable.book9,R.drawable.book10
        )

        ref = FirebaseDatabase.getInstance().getReference("Places")

        val placesList:ArrayList<Places> = ArrayList()

        fab_btn.setOnClickListener {
            showUpdateDialog()
        }

        ref!!.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                pd.dismiss()
            }
            override fun onDataChange(p0: DataSnapshot) {
                    placesList.clear()
                    for(postSnapshot in p0.children){
                        val place = postSnapshot.getValue(Places::class.java)
                        placesList.add(place!!)
                    }
                    val adapter = PlacesAdapter(this@Places_Rkv_Activity,R.layout.recycler_row,placesList, images)
                    listView!!.adapter = adapter
                    pd.dismiss()
                    //listView.addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
                }
        })

    }

    private fun showUpdateDialog() {
        val builder = AlertDialog.Builder(this@Places_Rkv_Activity)
        builder.setTitle("Add Place")
        val inflater = LayoutInflater.from(this@Places_Rkv_Activity)
        val view = inflater.inflate(R.layout.add_place, null)

        place = view.findViewById(R.id.place)
        currentTime = view.findViewById(R.id.time)
        status = view.findViewById(R.id.status)
        switch1 = view.findViewById(R.id.switch1)


        currentTime.text = getCurrentTime()
        status.text = "Closed"
        builder.setView(view)


        switch1.setOnCheckedChangeListener(this)
        newPlace = place.text.toString()
        status.text = stat
        currentTime.text = time

        //val id:String = ref.push().key!!
        //val new = Places(id,newPlace, stat, time)
        builder.setPositiveButton("Upload"
        ) { dialog, which ->

            //savePlace(newPlace,stat,time)
            if(place.text.toString().isNotEmpty()){
                val id:String = ref!!.push().key!!
                val new = Places(id, place.text.toString(), stat, time)
                val ref = FirebaseDatabase.getInstance().getReference("Places")
                ref.child(id).setValue(new)
                Toast.makeText(this@Places_Rkv_Activity, "Place Added", Toast.LENGTH_SHORT).show()

            }
            else{
                place.error = "Please Enter a Place"
                place.isFocusable = true
                Toast.makeText(this@Places_Rkv_Activity, "Place Not Added", Toast.LENGTH_SHORT).show()
            }
            /*val refPlace = FirebaseDatabase.getInstance().getReference("Places")
            val newStatus:String = place.text.toString()
            val id:String = oldVal[0].id
            val newPlace = Places(id, placeText.toString(), stat, time)
            refPlace.child(id).setValue(newPlace).addOnCompleteListener{
                Toast.makeText(mCtx, "Status Updated", Toast.LENGTH_SHORT).show()
            }*/
        }
        builder.setNegativeButton("No"
        ) { dialog, which ->
        }

        val alert = builder.create()
        alert.show()
    }
    override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) =
        if (switch1.isChecked) {
            status.text = "Open"
            stat = "Open"
            time = getCurrentTime()
            currentTime.text = time
        } else {
            status.text = "Closed"
            stat = "Closed"
            time = getCurrentTime()
            currentTime.text = time
        }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentTime():String{
        val calendar = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("hh:mm:ss a \tdd-MMM")
        time = simpleDateFormat.format(calendar.time)
        return time
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

