package com.example.rkvconnexa2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.cardview.widget.CardView

class FacultyActivity : AppCompatActivity(), View.OnClickListener{

    private var index:Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faculty)

        val actionBar = supportActionBar
        actionBar!!.title = "Select Department"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayShowHomeEnabled(true)

        val click1 = findViewById<CardView>(R.id.click1)
        val click2 = findViewById<CardView>(R.id.click2)
        val click3 = findViewById<CardView>(R.id.click3)
        val click4 = findViewById<CardView>(R.id.click4)
        val click5 = findViewById<CardView>(R.id.click5)
        val click6 = findViewById<CardView>(R.id.click6)
        val click7 = findViewById<CardView>(R.id.click7)
        val click8 = findViewById<CardView>(R.id.click8)
        val click9 = findViewById<CardView>(R.id.click9)
        val click10 = findViewById<CardView>(R.id.click10)
        val click11 = findViewById<CardView>(R.id.click11)
        val click12 = findViewById<CardView>(R.id.click12)
        val click13 = findViewById<CardView>(R.id.click13)
        val click14 = findViewById<CardView>(R.id.click14)
        val click15 = findViewById<CardView>(R.id.click15)
        val click16 = findViewById<CardView>(R.id.click16)
        val click17 = findViewById<CardView>(R.id.click17)

        click1.setOnClickListener(this)
        click2.setOnClickListener(this)
        click3.setOnClickListener(this)
        click4.setOnClickListener(this)
        click5.setOnClickListener(this)
        click6.setOnClickListener(this)
        click7.setOnClickListener(this)
        click8.setOnClickListener(this)
        click9.setOnClickListener(this)
        click10.setOnClickListener(this)
        click11.setOnClickListener(this)
        click12.setOnClickListener(this)
        click13.setOnClickListener(this)
        click14.setOnClickListener(this)
        click15.setOnClickListener(this)
        click16.setOnClickListener(this)
        click17.setOnClickListener(this)


    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.click1 ->{ index = 5}
            R.id.click2 ->{ index = 6}
            R.id.click3 ->{ index = 7}
            R.id.click4 ->{ index = 8}
            R.id.click5 ->{ index = 9}
            R.id.click6 ->{ index = 10}
            R.id.click7 ->{ index = 11}
            R.id.click8 ->{ index = 12}
            R.id.click9 ->{ index = 13}
            R.id.click10 ->{ index = 14}
            R.id.click11 ->{ index = 15}
            R.id.click12 ->{ index = 16}
            R.id.click13 ->{ index = 17}
            R.id.click14 ->{ index = 18}
            R.id.click15 ->{ index = 19}
            R.id.click16 ->{ index = 20}
            R.id.click17 ->{ index = 21}
        }
        val intent = Intent(this@FacultyActivity, PdfView::class.java)
        intent.putExtra("index", index)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

